use num_traits::{FromPrimitive, ToPrimitive};
use rand::distributions::weighted::alias_method::WeightedIndex;
use rand::prelude::*;
use std::ops::{Deref, DerefMut};

type DistributionList = Vec<WeightedIndex<f32>>;

/// Trait for describing the number of variants in an enum.
pub trait EnumVariantCount {
    const VARIANT_COUNT: usize;
}

/// Represents each of the possible things a Derp can see in front of itself.
#[derive(FromPrimitive, ToPrimitive)]
pub enum Sight {
    Empty = 0,
    Food,
    Derp,
}

impl EnumVariantCount for Sight {
    const VARIANT_COUNT: usize = 3;
}

/// Represents each of the potential actions a Derp can perform.
#[derive(FromPrimitive)]
pub enum Action {
    MoveForward = 0,
    TurnBack,
    TurnLeft,
    TurnRight,
}

impl EnumVariantCount for Action {
    const VARIANT_COUNT: usize = 4;
}

/// Structure containing all the probability distributions that a Derp uses to select its next
/// action based on what it sees and what mode it's in.
#[derive(Debug)]
pub struct ActionMatrix(DistributionList);

impl From<DistributionList> for ActionMatrix {
    fn from(list: DistributionList) -> ActionMatrix {
        ActionMatrix(list)
    }
}

impl Deref for ActionMatrix {
    type Target = DistributionList;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for ActionMatrix {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Structure containing all the probability distributions that a Derp uses to select its next mode
/// baesd on what it sees and what mode it's currently in.
#[derive(Debug)]
pub struct ModeMatrix(DistributionList);

impl From<DistributionList> for ModeMatrix {
    fn from(list: DistributionList) -> ModeMatrix {
        ModeMatrix(list)
    }
}

impl Deref for ModeMatrix {
    type Target = DistributionList;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for ModeMatrix {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Numeric tag indicating what "mode" a Derp is currently in.
///
/// The simplest version of a Derp brain has a single mode. Its next action is sampled from a
/// probability distribution based on what it currently sees in front of it. More complicated Derps
/// have multiple modes in the sense that they have multiple action distributions per sight (one
/// distribution per mode). Derps with multiple modes not only select their next action based on
/// what they currently see, but also their next mode.
#[allow(dead_code)]
pub type Mode = usize;

/// Trait defining the functions of a matrix.
///
/// A matrix is a table of discrete probability distributions for sampling an output, indexed
/// by mode and input.
pub trait Matrix<Input, Output>
where
    Self: From<DistributionList> + Deref<Target = DistributionList> + DerefMut,
    Input: EnumVariantCount + FromPrimitive + ToPrimitive,
    Output: FromPrimitive,
{
    // Function for computing the weight of each probability in a perfectly uniform matrix
    fn uniform_weight(mode_count: usize) -> f32;

    /// Creates a matrix out of entirely uniform probability distributions.
    fn uniform(mode_count: usize) -> Self {
        let mut matrix = Vec::with_capacity(mode_count + Input::VARIANT_COUNT);
        for _ in 0..(mode_count * Input::VARIANT_COUNT) {
            let weight = Self::uniform_weight(mode_count);
            let weights = vec![weight; Input::VARIANT_COUNT];
            matrix.push(WeightedIndex::new(weights).unwrap());
        }
        Self::from(matrix)
    }

    /// Cretes a matrix out of a probability distribution with random weights.
    fn random<R: Rng + ?Sized>(mode_count: usize, rng: &mut R) -> Self {
        let mut matrix = Vec::with_capacity(mode_count + Input::VARIANT_COUNT);
        for _ in 0..(mode_count * Input::VARIANT_COUNT) {
            let weight = rng.gen();
            let weights = vec![weight; Input::VARIANT_COUNT];
            matrix.push(WeightedIndex::new(weights).unwrap());
        }
        Self::from(matrix)
    }

    /// Samples this matrix given a mode and input.
    fn sample<R: Rng + ?Sized>(&self, mode: Mode, input: Input, mut rng: &mut R) -> Output {
        let input_num = Input::to_usize(&input).unwrap();
        let distribution = &(**self)[Input::VARIANT_COUNT * mode + input_num];
        Output::from_usize(distribution.sample(&mut rng)).unwrap()
    }
}

impl Matrix<Sight, Action> for ActionMatrix {
    #[allow(unused_variables)]
    fn uniform_weight(mode_count: usize) -> f32 {
        Action::VARIANT_COUNT as f32
    }
}

impl Matrix<Sight, Mode> for ModeMatrix {
    fn uniform_weight(mode_count: usize) -> f32 {
        mode_count as f32
    }
}
