use termion::event::Key;
use tui::{
    backend::Backend,
    layout::Rect,
    style::{Color, Style},
    text::Span,
    widgets::{Block, Borders, List, ListItem, Paragraph},
    Frame,
};

use unicode_width::UnicodeWidthStr;

use crate::{constants::*, draw::Draw, event::Event};

pub struct LogBuffer(Vec<String>);

impl Default for LogBuffer {
    fn default() -> LogBuffer {
        let vec = Vec::new();
        LogBuffer(vec)
    }
}

impl LogBuffer {
    pub fn push(&mut self, text: String) {
        let LogBuffer(ref mut buffer) = self;
        buffer.push(text);
    }

    pub fn tail(&self, last_n: usize) -> &[String] {
        let LogBuffer(ref buffer) = self;
        let len = buffer.len();
        let last_n = if last_n > len { len } else { last_n };
        &buffer[len - last_n..]
    }

    pub fn len(&self) -> usize {
        let LogBuffer(ref buffer) = self;
        buffer.len()
    }
}

impl Draw for LogBuffer {
    fn draw<B>(&self, f: &mut Frame<B>, area: Rect)
    where
        B: Backend,
    {
        let entries: Vec<ListItem> = {
            let line_count = area.height - 2;
            let log_length = self.len() as u16;
            let log_lines = self
                .tail(line_count as usize)
                .iter()
                .map(|e| ListItem::new(Span::raw(e)));
            if log_length < line_count {
                let blank_lines =
                    (0..line_count - log_length).map(|_| ListItem::new(Span::raw("")));
                blank_lines.chain(log_lines).collect()
            } else {
                log_lines.collect()
            }
        };
        let log_block =
            List::new(entries).block(Block::default().title("Log").borders(Borders::ALL));
        f.render_widget(log_block, area);
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum InputMode {
    Normal,
    Editing,
}

impl Default for InputMode {
    fn default() -> InputMode {
        InputMode::Normal
    }
}

pub struct InputBar(String, InputMode);

impl InputBar {
    pub fn mode(&self) -> InputMode {
        self.1
    }

    pub fn pop(&mut self) -> Option<char> {
        self.0.pop()
    }

    pub fn push(&mut self, ch: char) {
        self.0.push(ch);
    }

    pub fn drain<R>(&mut self, range: R) -> std::string::Drain
    where
        R: std::ops::RangeBounds<usize>,
    {
        self.0.drain(range)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn toggle_mode(&mut self) {
        self.1 = match self.1 {
            InputMode::Normal => InputMode::Editing,
            InputMode::Editing => InputMode::Normal,
        };
    }
}

impl Draw for InputBar {
    fn draw<B>(&self, f: &mut Frame<B>, area: Rect)
    where
        B: Backend,
    {
        let InputBar(input, mode) = self;

        let input_block = Paragraph::new(input.as_ref())
            .style(match mode {
                InputMode::Normal => Style::default(),
                // Highlight the input block's foreground if the app is in edit mode
                InputMode::Editing => Style::default().fg(Color::Yellow),
            })
            .block(Block::default().borders(Borders::ALL).title("Input"));
        f.render_widget(input_block, area);

        // Draw cursor if the app is in edit mode
        match mode {
            InputMode::Normal => {}
            InputMode::Editing => f.set_cursor(area.x + input.width() as u16 + 1, area.y + 1),
        }
    }
}

impl Default for InputBar {
    fn default() -> InputBar {
        InputBar(String::new(), InputMode::default())
    }
}

type ShouldContiue = bool;

pub struct App {
    is_paused: bool,
    tick: usize,
    input: InputBar,
    log: LogBuffer,
}

impl Default for App {
    fn default() -> App {
        App {
            is_paused: false,
            tick: 0usize,
            input: InputBar::default(),
            log: LogBuffer::default(),
        }
    }
}

impl App {
    pub fn log(&self) -> &LogBuffer {
        &self.log
    }

    pub fn input(&self) -> &InputBar {
        &self.input
    }

    pub fn handle_event(&mut self, event: Event<Key>) -> ShouldContiue {
        match event {
            Event::Input(key) => match key {
                KEY_EXIT => {
                    return false;
                }
                KEY_ENTER => match self.input.mode() {
                    InputMode::Normal => {
                        self.input.toggle_mode();
                    }
                    InputMode::Editing => {
                        if self.input.len() != 0 {
                            self.log.push(format!(
                                "Tick {}: {}",
                                self.tick,
                                self.input.drain(..).collect::<String>()
                            ));
                        }
                    }
                },
                KEY_ESC => {
                    if self.input.mode() == InputMode::Editing {
                        self.input.toggle_mode();
                    }
                }
                KEY_BACKSPACE => {
                    if self.input.mode() == InputMode::Editing {
                        self.input.pop();
                    }
                }
                Key::Char(c) => match self.input.mode() {
                    InputMode::Normal => {
                        if key == KEY_P {
                            self.is_paused = !self.is_paused;
                        }
                    }
                    InputMode::Editing => {
                        self.input.push(c);
                    }
                },
                _ => {}
            },
            Event::Tick => {
                if !self.is_paused {
                    self.tick += 1;
                }
            }
        }

        true
    }
}
