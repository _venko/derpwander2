use std::time::Duration;
use termion::event::Key;

pub const TICK_RATE: Duration = Duration::from_millis(100);

pub const KEY_EXIT: Key = Key::Ctrl('c');
pub const KEY_ENTER: Key = Key::Char('\n');
pub const KEY_ESC: Key = Key::Esc;
pub const KEY_BACKSPACE: Key = Key::Backspace;
pub const KEY_P: Key = Key::Char('p');
