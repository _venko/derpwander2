#[macro_use]
extern crate num_derive;

mod app;
mod constants;
mod draw;
mod event;
mod matrix;

use app::App;
use constants::*;
use draw::Draw;
use event::{Config, Events};

use std::{error::Error, io};
use termion::{input::MouseTerminal, raw::IntoRawMode, screen::AlternateScreen};
use tui::{
    backend::{TermionBackend},
    layout::{Constraint, Direction, Layout},
    widgets::{Block, BorderType, Borders},
    Terminal,
};

fn main() -> Result<(), Box<dyn Error>> {
    // Terminal configuration
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Set up event handlers
    let events = Events::with_config(Config {
        exit_key: KEY_EXIT,
        tick_rate: TICK_RATE,
    });

    let mut app = App::default();

    'app: loop {
        terminal.draw(|frame| {
            let size = frame.size();

            // Draw a block the size of the terminal.
            let block = Block::default()
                .title("DerpUI")
                .borders(Borders::ALL)
                .border_type(BorderType::Rounded);
            frame.render_widget(block, size);

            let (log_frame, _brain_frame, input_frame) = {
                let v_frames = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(2)
                    .constraints([Constraint::Min(1), Constraint::Length(3)].as_ref())
                    .split(frame.size());

                let h_frames = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(v_frames[0]);
                (h_frames[0], h_frames[1], v_frames[1])
            };

            let log = app.log();
            let input = app.input();
            (*log).draw(frame, log_frame);
            (*input).draw(frame, input_frame);
        })?;

        let event = events.next()?;
        if !app.handle_event(event) {
            break 'app;
        }
    }

    Ok(())
}
