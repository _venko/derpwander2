use tui::{backend::Backend, layout::Rect, Frame};

/// A trait for drawing a window within the larger TUI.
///
/// TODO: This probably can be accomplished with rust-tui's Widget type
pub trait Draw {
    fn draw<B>(&self, f: &mut Frame<B>, area: Rect)
    where
        B: Backend;
}
